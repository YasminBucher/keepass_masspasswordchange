﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassPasswordChange
{
	class MagicStrings
	{
		private MagicStrings(){}

		public const string PLACEHOLDER_PATH = "{{PATH}}";
		public const string PLACEHOLDER_USER = "{{USER}}";
		public const string PLACEHOLDER_NEWPASS = "{{PASS}}";
		public const string PLACEHOLDER_OLDPASS = "{{OLDPASS}}";
	}
}