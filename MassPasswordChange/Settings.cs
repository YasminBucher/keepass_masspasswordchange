﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MassPasswordChange
{
	class Settings
	{
		private static string settingsFilePath = string.Empty;
		private static Settings instance = null;

		public static bool WriteChangesToFile
		{
			get
			{
				return instance._writeChangesToFile;
			}
		}

		public static string OutputFilePath
		{
			get
			{
				return instance._outputFilePath;
			}
		}

		public static string FileEntryFormat
		{
			get
			{
				return instance._fileEntryFormat;
			}
		}

		public static string ProfileName
		{
			get
			{
				return instance._profileName;
			}
		}

		private bool _writeChangesToFile;
		private string _outputFilePath;
		private string _fileEntryFormat;
		private string _profileName;

		static Settings()
		{
			settingsFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Binary Overdrive", "KeePass Plugins", "MassPasswordChange", "settings.xml");

			string settingsFileDirectory = Path.GetDirectoryName(settingsFilePath);

			if(!Directory.Exists(settingsFileDirectory))
			{
				Directory.CreateDirectory(settingsFileDirectory);
			}
		}

		private Settings()
		{
			this._fileEntryFormat = "Username: {{USER}}, Password: {{PASS}}";
		}

		internal static void SetValues(bool writeChangesToFile, string outputFilePath, string fileEntryFormat, string profileName)
		{
			instance._writeChangesToFile = writeChangesToFile;
			instance._outputFilePath = outputFilePath;
			instance._fileEntryFormat = fileEntryFormat;
			instance._profileName = profileName;
		}

		public static void Save()
		{
			XmlSerializer ser = new XmlSerializer(typeof(Settings));

			using(FileStream fs = new FileStream(settingsFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				ser.Serialize(fs, instance);
			}
		}

		public static void Load()
		{
			if(File.Exists(settingsFilePath))
			{
				XmlSerializer ser = new XmlSerializer(typeof(Settings));

				using(FileStream fs = new FileStream(settingsFilePath, FileMode.Open, FileAccess.Read, FileShare.None))
				{
					instance = (Settings)ser.Deserialize(fs);
				}
			}
			else
			{
				instance = new Settings();
			}
		}
	}
}