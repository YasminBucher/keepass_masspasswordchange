﻿namespace MassPasswordChange
{
	partial class MassPasswordChangeWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cmbProfile = new System.Windows.Forms.ComboBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblProfile = new System.Windows.Forms.Label();
			this.btnSelectOutputFile = new System.Windows.Forms.Button();
			this.chkOutputChangesToFile = new System.Windows.Forms.CheckBox();
			this.txtOutputFile = new System.Windows.Forms.TextBox();
			this.lblOutputFile = new System.Windows.Forms.Label();
			this.lblEntrySelection = new System.Windows.Forms.Label();
			this.lblFileEntryFormat = new System.Windows.Forms.Label();
			this.txtFileEntryFormat = new System.Windows.Forms.TextBox();
			this.btnPlaceholderSelection = new System.Windows.Forms.Button();
			this.cmsPlaceholderSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.ctvEntrySelection = new MassPasswordChange.Controls.CheckedTreeView();
			this.SuspendLayout();
			// 
			// cmbProfile
			// 
			this.cmbProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cmbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbProfile.FormattingEnabled = true;
			this.cmbProfile.Location = new System.Drawing.Point(51, 333);
			this.cmbProfile.Name = "cmbProfile";
			this.cmbProfile.Size = new System.Drawing.Size(275, 21);
			this.cmbProfile.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Location = new System.Drawing.Point(170, 435);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "Abbrechen";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.Location = new System.Drawing.Point(251, 435);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblProfile
			// 
			this.lblProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblProfile.AutoSize = true;
			this.lblProfile.Location = new System.Drawing.Point(12, 336);
			this.lblProfile.Name = "lblProfile";
			this.lblProfile.Size = new System.Drawing.Size(33, 13);
			this.lblProfile.TabIndex = 3;
			this.lblProfile.Text = "Profil:";
			// 
			// btnSelectOutputFile
			// 
			this.btnSelectOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelectOutputFile.Location = new System.Drawing.Point(299, 383);
			this.btnSelectOutputFile.Name = "btnSelectOutputFile";
			this.btnSelectOutputFile.Size = new System.Drawing.Size(27, 20);
			this.btnSelectOutputFile.TabIndex = 5;
			this.btnSelectOutputFile.Text = "...";
			this.btnSelectOutputFile.UseVisualStyleBackColor = true;
			this.btnSelectOutputFile.Click += new System.EventHandler(this.btnSelectOutputFile_Click);
			// 
			// chkOutputChangesToFile
			// 
			this.chkOutputChangesToFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.chkOutputChangesToFile.AutoSize = true;
			this.chkOutputChangesToFile.Location = new System.Drawing.Point(12, 360);
			this.chkOutputChangesToFile.Name = "chkOutputChangesToFile";
			this.chkOutputChangesToFile.Size = new System.Drawing.Size(178, 17);
			this.chkOutputChangesToFile.TabIndex = 6;
			this.chkOutputChangesToFile.Text = "Änderungen in Datei schreiben?";
			this.chkOutputChangesToFile.UseVisualStyleBackColor = true;
			this.chkOutputChangesToFile.CheckedChanged += new System.EventHandler(this.chkOutputChangesToFile_CheckedChanged);
			// 
			// txtOutputFile
			// 
			this.txtOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtOutputFile.Location = new System.Drawing.Point(60, 383);
			this.txtOutputFile.Name = "txtOutputFile";
			this.txtOutputFile.Size = new System.Drawing.Size(233, 20);
			this.txtOutputFile.TabIndex = 7;
			// 
			// lblOutputFile
			// 
			this.lblOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblOutputFile.AutoSize = true;
			this.lblOutputFile.Location = new System.Drawing.Point(12, 386);
			this.lblOutputFile.Name = "lblOutputFile";
			this.lblOutputFile.Size = new System.Drawing.Size(35, 13);
			this.lblOutputFile.TabIndex = 8;
			this.lblOutputFile.Text = "Datei:";
			// 
			// lblEntrySelection
			// 
			this.lblEntrySelection.AutoSize = true;
			this.lblEntrySelection.Location = new System.Drawing.Point(12, 9);
			this.lblEntrySelection.Name = "lblEntrySelection";
			this.lblEntrySelection.Size = new System.Drawing.Size(113, 13);
			this.lblEntrySelection.TabIndex = 9;
			this.lblEntrySelection.Text = "Zu ändernde Einträge:";
			// 
			// lblFileEntryFormat
			// 
			this.lblFileEntryFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblFileEntryFormat.AutoSize = true;
			this.lblFileEntryFormat.Location = new System.Drawing.Point(12, 412);
			this.lblFileEntryFormat.Name = "lblFileEntryFormat";
			this.lblFileEntryFormat.Size = new System.Drawing.Size(42, 13);
			this.lblFileEntryFormat.TabIndex = 12;
			this.lblFileEntryFormat.Text = "Format:";
			// 
			// txtFileEntryFormat
			// 
			this.txtFileEntryFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtFileEntryFormat.Location = new System.Drawing.Point(60, 409);
			this.txtFileEntryFormat.Name = "txtFileEntryFormat";
			this.txtFileEntryFormat.Size = new System.Drawing.Size(233, 20);
			this.txtFileEntryFormat.TabIndex = 11;
			// 
			// btnPlaceholderSelection
			// 
			this.btnPlaceholderSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPlaceholderSelection.Location = new System.Drawing.Point(299, 409);
			this.btnPlaceholderSelection.Name = "btnPlaceholderSelection";
			this.btnPlaceholderSelection.Size = new System.Drawing.Size(27, 20);
			this.btnPlaceholderSelection.TabIndex = 13;
			this.btnPlaceholderSelection.Text = "v";
			this.btnPlaceholderSelection.UseVisualStyleBackColor = true;
			this.btnPlaceholderSelection.Click += new System.EventHandler(this.btnPlaceholderSelection_Click);
			// 
			// cmsPlaceholderSelection
			// 
			this.cmsPlaceholderSelection.Name = "cmsPlaceholderSelection";
			this.cmsPlaceholderSelection.Size = new System.Drawing.Size(61, 4);
			// 
			// ctvEntrySelection
			// 
			this.ctvEntrySelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ctvEntrySelection.CheckBoxes = true;
			this.ctvEntrySelection.Location = new System.Drawing.Point(12, 25);
			this.ctvEntrySelection.Name = "ctvEntrySelection";
			this.ctvEntrySelection.Size = new System.Drawing.Size(314, 302);
			this.ctvEntrySelection.TabIndex = 10;
			// 
			// MassPasswordChangeWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(338, 470);
			this.Controls.Add(this.btnPlaceholderSelection);
			this.Controls.Add(this.lblFileEntryFormat);
			this.Controls.Add(this.txtFileEntryFormat);
			this.Controls.Add(this.ctvEntrySelection);
			this.Controls.Add(this.lblEntrySelection);
			this.Controls.Add(this.lblOutputFile);
			this.Controls.Add(this.txtOutputFile);
			this.Controls.Add(this.chkOutputChangesToFile);
			this.Controls.Add(this.btnSelectOutputFile);
			this.Controls.Add(this.lblProfile);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cmbProfile);
			this.Name = "MassPasswordChangeWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Mass Password Change";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cmbProfile;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Label lblProfile;
		private System.Windows.Forms.Button btnSelectOutputFile;
		private System.Windows.Forms.CheckBox chkOutputChangesToFile;
		private System.Windows.Forms.TextBox txtOutputFile;
		private System.Windows.Forms.Label lblOutputFile;
		private System.Windows.Forms.Label lblEntrySelection;
		private Controls.CheckedTreeView ctvEntrySelection;
		private System.Windows.Forms.Label lblFileEntryFormat;
		private System.Windows.Forms.TextBox txtFileEntryFormat;
		private System.Windows.Forms.Button btnPlaceholderSelection;
		private System.Windows.Forms.ContextMenuStrip cmsPlaceholderSelection;
	}
}