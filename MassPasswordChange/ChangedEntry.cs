﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassPasswordChange
{
	class ChangedEntry
	{
		public string Path { get; set; }
		public string Username { get; set; }
		public string NewPassword { get; set; }
		public string OldPassword { get; set; }

		public ChangedEntry(string path, string username, string newPassword, string oldPassword)
		{
			this.Path = path;
			this.Username = username;
			this.NewPassword = newPassword;
			this.OldPassword = oldPassword;
		}
	}
}