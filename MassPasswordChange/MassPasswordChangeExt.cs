﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using KeePass;
using KeePass.Plugins;
using KeePassLib;
using KeePassLib.Cryptography.PasswordGenerator;
using KeePassLib.Security;
using System.Linq;
using System.Security.Principal;

namespace MassPasswordChange
{
	public class MassPasswordChangeExt : Plugin
	{
		private const int PWHISTORY_MAXENTRIES = 8;
		private const string STRINGNAME_PWHISTORY = "Password History";

		private IPluginHost pluginHost = null;
		private ToolStripItemCollection tsMenu = null;
		private ToolStripSeparator tssPreMassPasswordChange = null;
		private ToolStripMenuItem tsmiMassPasswordChange = null;

		public override string UpdateUrl
		{
			get
			{
				return "https://bitbucket.org/KarillEndusa/keepass-masspasswordchange/src/master/MassPasswordChange/version.txt";
			}
		}

		public override bool Initialize(IPluginHost host)
		{
			this.pluginHost = host;

			Settings.Load();

			this.tsMenu = this.pluginHost.MainWindow.ToolsMenu.DropDownItems;

			this.tssPreMassPasswordChange = new ToolStripSeparator();
			this.tsMenu.Add(this.tssPreMassPasswordChange);

			this.tsmiMassPasswordChange = new ToolStripMenuItem("MassPasswordChange");
			this.tsmiMassPasswordChange.Click += this.tsmiMassPasswordChange_Click;
			this.tsMenu.Add(this.tsmiMassPasswordChange);

			return true;
		}

		public override void Terminate()
		{
			Settings.Save();

			this.tsmiMassPasswordChange.Click -= this.tsmiMassPasswordChange_Click;
			this.tsMenu.Remove(this.tssPreMassPasswordChange);
			this.tsMenu.Remove(this.tsmiMassPasswordChange);
		}

		void tsmiMassPasswordChange_Click(object sender, EventArgs e)
		{
			this.ChangePassword();
		}

		private void ChangePassword()
		{
			if(this.pluginHost.Database == null || !this.pluginHost.Database.IsOpen)
			{
				Util.ShowInformationBox("This plugin needs an opened database to work properly.");
				return;
			}

			List<PwEntry> entries = this.GetEntriesToChange();

			if(entries != null && entries.Count > 0)
			{
				this.SetNewPasswords(entries);
			}
		}

		private void SetNewPasswords(List<PwEntry> entries)
		{
			List<ChangedEntry> changedEntries = new List<ChangedEntry>();

			foreach(PwEntry entry in entries)
			{
				string path = entry.ParentGroup.GetFullPath(">", true);
				string username = entry.Strings.ReadSafe(PwDefs.TitleField);
				string oldPassword = entry.Strings.ReadSafe(PwDefs.PasswordField);

				this.UpdatePasswordHistory(entry);
				this.SetNewPassword(entry);

				string newPassword = entry.Strings.ReadSafe(PwDefs.PasswordField);

				changedEntries.Add(new ChangedEntry(path, username, newPassword, oldPassword));
			}

			// Generate File with new passwords
			if(Settings.WriteChangesToFile)
			{
				this.GeneratePasswordFile(changedEntries);
			}

			// Tell KeePass that data has been modified and needs to be saved
			pluginHost.Database.Modified = true;
		}

		private void GeneratePasswordFile(List<ChangedEntry> changedEntries)
		{
			TextReplacementManager trm = new TextReplacementManager();

			string format = Settings.FileEntryFormat;

			using(FileStream fs = new FileStream(Settings.OutputFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				using(StreamWriter sw = new StreamWriter(fs))
				{
					foreach(ChangedEntry changedEntry in changedEntries)
					{
						trm.Set(MagicStrings.PLACEHOLDER_PATH, changedEntry.Path);
						trm.Set(MagicStrings.PLACEHOLDER_USER, changedEntry.Username);
						trm.Set(MagicStrings.PLACEHOLDER_NEWPASS, changedEntry.NewPassword);
						trm.Set(MagicStrings.PLACEHOLDER_OLDPASS, changedEntry.OldPassword);

						sw.WriteLine(trm.PerformReplacements(format));
					}
				}
			}
		}

		private void SetNewPassword(PwEntry entry)
		{
			ProtectedString ps;
			PwProfile profile = Util.GetProfiles().FirstOrDefault(p => p.Name == Settings.ProfileName);
			PwGenerator.Generate(out ps, profile, null, Program.PwGeneratorPool);
			entry.Strings.Set(PwDefs.PasswordField, ps);
		}

		private void UpdatePasswordHistory(PwEntry entry)
		{
			string oldHistoryValue = "";

			if(entry.Strings.Exists(STRINGNAME_PWHISTORY))
			{
				oldHistoryValue = entry.Strings.ReadSafe(STRINGNAME_PWHISTORY);
			}

			List<string> historyLines = new List<string>(oldHistoryValue.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
			string currentUser = WindowsIdentity.GetCurrent().Name;
			string historyEntry = string.Format(
				"{0} (valid through: {1:dd.MM.yyyy}) (changed by: {2})",
				entry.Strings.ReadSafe(PwDefs.PasswordField),
				DateTime.Today,
				currentUser);
			historyLines.Add(historyEntry);

			if(historyLines.Count > PWHISTORY_MAXENTRIES)
			{
				historyLines.RemoveAt(0);
			}

			string newHistoryValue = string.Join(Environment.NewLine, historyLines);
			ProtectedString psPasswordHistory = new ProtectedString(false, newHistoryValue);
			entry.Strings.Set(STRINGNAME_PWHISTORY, psPasswordHistory);
		}

		private List<PwEntry> GetEntriesToChange()
		{
			using(MassPasswordChangeWindow selectProfileWindow = new MassPasswordChangeWindow(this.pluginHost.Database.RootGroup))
			{
				DialogResult dr = selectProfileWindow.ShowDialog();

				if(dr == DialogResult.OK)
				{
					return selectProfileWindow.SelectedEntries;
				}
				else
				{
					return null;
				}
			}
		}
	}
}