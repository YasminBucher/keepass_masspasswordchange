﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace MassPasswordChange.Controls
{
	class CheckedTreeView : TreeView
	{
		private ImageList _stateImages = new ImageList();
		private bool _useTriState = true;
		private bool _checkBoxesVisible;
		private bool _preventCheckEvent;

		/// <summary>
		/// Gets or sets to display
		/// checkboxes in the tree
		/// view.
		/// </summary>
		[Category("Appearance")]
		[Description("Sets tree view to display checkboxes or not.")]
		[DefaultValue(false)]
		public new bool CheckBoxes
		{
			get { return _checkBoxesVisible; }
			set
			{
				this._checkBoxesVisible = value;
				base.CheckBoxes = this._checkBoxesVisible;
				this.StateImageList = this._checkBoxesVisible ? this._stateImages : null;
			}
		}

		[Browsable(false)]
		public new ImageList StateImageList
		{
			get { return base.StateImageList; }
			set { base.StateImageList = value; }
		}

		/// <summary>
		/// Gets or sets to support
		/// tri-state in the checkboxes
		/// or not.
		/// </summary>
		[Category("Appearance")]
		[Description("Sets tree view to use tri-state checkboxes or not.")]
		[DefaultValue(true)]
		public bool CheckBoxesTriState
		{
			get { return this._useTriState; }
			set { this._useTriState = value; }
		}

		public CheckedTreeView()
			: base()
		{
			CheckBoxState state = CheckBoxState.UncheckedNormal;

			Graphics g;
			Bitmap b;

			for(int i = 0; i < 3; i++)
			{
				b = new Bitmap(16, 16);
				g = Graphics.FromImage(b);

				switch(i)
				{
					case 0:
						state = CheckBoxState.UncheckedNormal;
						break;
					case 1:
						state = CheckBoxState.CheckedNormal;
						break;
					case 2:
						state = CheckBoxState.MixedNormal;
						break;
				}

				CheckBoxRenderer.DrawCheckBox(g, new Point(2, 2), state);
				g.Save();
				this._stateImages.Images.Add(b);
			}
		}

		public override void Refresh()
		{
			base.Refresh();

			if(!this.CheckBoxes)
			{
				return;
			}

			base.CheckBoxes = false;

			Stack<TreeNode> nodes = new Stack<TreeNode>(this.Nodes.Count);

			foreach(TreeNode node in this.Nodes)
			{
				nodes.Push(node);
			}

			TreeNode stackedNode;

			while(nodes.Count > 0)
			{
				stackedNode = nodes.Pop();

				if(stackedNode.StateImageIndex == -1)
				{
					stackedNode.StateImageIndex = stackedNode.Checked ? 1 : 0;
				}

				foreach(TreeNode childNode in stackedNode.Nodes)
				{
					nodes.Push(childNode);
				}
			}
		}

		protected override void OnLayout(LayoutEventArgs levent)
		{
			base.OnLayout(levent);
			this.Refresh();
		}

		private Bitmap GetCheckBoxBitmap(CheckBoxState state)
		{
			Bitmap bmp = new Bitmap(16, 16);
			Graphics g = Graphics.FromImage(bmp);
			CheckBoxRenderer.DrawCheckBox(g, new Point(2, 2), state);
			g.Save();

			return bmp;
		}

		protected override void OnAfterExpand(TreeViewEventArgs e)
		{
			base.OnAfterExpand(e);

			foreach(TreeNode childNode in e.Node.Nodes)
			{
				if(childNode.StateImageIndex == -1)
				{
					childNode.StateImageIndex = childNode.Checked ? 1 : 0;
				}
			}
		}

		protected override void OnAfterCheck(TreeViewEventArgs e)
		{
			base.OnAfterCheck(e);

			if(this._preventCheckEvent)
			{
				return;
			}

			this.OnNodeMouseClick(new TreeNodeMouseClickEventArgs(e.Node, MouseButtons.None, 0, 0, 0));
		}

		protected override void OnNodeMouseClick(TreeNodeMouseClickEventArgs e)
		{
			base.OnNodeMouseClick(e);

			this._preventCheckEvent = true;

			int spacing = ImageList == null ? 0 : 18;
			if((e.X > e.Node.Bounds.Left - spacing || e.X < e.Node.Bounds.Left - (spacing + 16)) && e.Button != MouseButtons.None)
			{
				this._preventCheckEvent = false;
				return;
			}

			TreeNode bufferNode = e.Node;

			if(e.Button == MouseButtons.Left)
			{
				bufferNode.Checked = !bufferNode.Checked;
			}

			if(bufferNode.Checked)
			{
				bufferNode.StateImageIndex = 1;
			}

			this.OnAfterCheck(new TreeViewEventArgs(bufferNode, TreeViewAction.ByMouse));

			Stack<TreeNode> nodes = new Stack<TreeNode>(bufferNode.Nodes.Count);
			nodes.Push(bufferNode);

			do
			{
				bufferNode = nodes.Pop();
				bufferNode.Checked = e.Node.Checked;

				foreach(TreeNode childNode in bufferNode.Nodes)
				{
					nodes.Push(childNode);
				}
			} while(nodes.Count > 0);

			bool mixedState = false;
			bufferNode = e.Node;

			int index;

			while(bufferNode.Parent != null)
			{
				foreach(TreeNode childNode in bufferNode.Parent.Nodes)
				{
					mixedState |= (childNode.Checked != bufferNode.Checked | childNode.StateImageIndex == 2);
				}

				index = (int)Convert.ToUInt32(bufferNode.Checked);
				bufferNode.Parent.Checked = mixedState || (index > 0);

				if(mixedState)
				{
					bufferNode.Parent.StateImageIndex = CheckBoxesTriState ? 2 : 1;
				}
				else
				{
					bufferNode.Parent.StateImageIndex = index;
				}

				bufferNode = bufferNode.Parent;
			}

			this._preventCheckEvent = false;
		}
	}
}
